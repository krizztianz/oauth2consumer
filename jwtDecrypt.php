<?php

require './vendor/autoload.php';
use Firebase\JWT\JWT;

$publicKey = <<<EOD
-----BEGIN PUBLIC KEY-----
MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEA5LQiM5btJ7gNMlmjBFAV
Hj1p2JzzsOk/UsfHC3HsRpc5+G1JGovecHzXBimqPhJx7I0aBwWE8XWhA5BmqwIX
QBMp1NzRn2NiEOL0zDx1BQ863zgmHk/7NTGFwmX0r7PFy9Z4FKr5l+Rs3qG4ZYdC
DJZa1Oor0cC04Q5Z48H/NmSt8xhvYFsewvI6UxAZvXFlvJUyk/yD0cBrzkeyD0g+
M95YKuSZFwwGzbXvjyqT74L36sStj9FLzJXwUlm4C6VAdhMaAMZAl/4wguK+zF8W
z5RtGZwBWXCb3vQaA4oTrDepWKSOE1caBvVPQPYSW5cPI2ch1HKLIU629kf+v+3G
ff4WSTqDR6d0kR4kGiRxqSAK2trcJS+IaErGRuFpVb7rpe3wpY1zqYkYQdiN+HM+
uyjfIeeygpwH4xeT15PVdhR8YPoIOBKdgwIX7igryYQoRZOoYhQP7vVVl24Km7DG
cUAxd8LINQcF4QYvS1Gb6/uxecKXDe4EOZGE1sRJ33rI/E74NguDhRmlWZfPWEWW
aeSaTiUP2BguAyXlGwfv+btFLKUZK0kW6z6QGr5/NmSW2srbC7a+pgvaGsjfrRUA
gdwS7aFfogln9Wsfy/hKWnEtLWMjzS0C/mgzqu9VW3tHF/K6dqhPxoduWg/3A15K
foPNG0ctSikxDRiL0sglMJ8CAwEAAQ==
-----END PUBLIC KEY-----
EOD;

$jwt = '';

try {
    $decoded = JWT::decode($jwt, $publicKey, array('RS256'));

    /*
     NOTE: This will now be an object instead of an associative array. To get
     an associative array, you will need to cast it as such:
    */

    $decoded_array = (array) $decoded;
    //echo "Decode:\n" . print_r($decoded_array, true) . "\n";
    
    header('Content-Type: application/json');
    echo json_encode($decoded_array);
} catch (ExpiredException $exc) {
    echo "Token Expired!";
} catch (Exception $e){
    echo $e->getMessage();
}

?>