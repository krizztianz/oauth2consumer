<?php

require './vendor/autoload.php';

use GuzzleHttp\Client;

$client = new GuzzleHttp\Client(['base_uri' => 'http://172.31.9.118:8989/o2svr/public/oauth/']);

$response = $client->request('POST', 'token', [
    'form_params' => [
            'grant_type' => 'client_credentials',
            'client_id' => '', //Diisi client ID
            'client_secret' => '', //Diisi secret key
            'scope' => '*'
        ]
    ]);

$body = $response->getBody();
$content = $body->getContents();

//echo $body;
header('Content-Type: application/json');
echo $content

//echo "<pre>";
//echo print_r($response->getBody()->getContents());
//echo "</pre>";

?>