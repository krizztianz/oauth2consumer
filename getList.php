<?php

require './vendor/autoload.php';

use GuzzleHttp\Client;

$client = new GuzzleHttp\Client(['base_uri' => 'http://172.31.9.118:8989/o2svr/public/api/']);
$token = ''; //Di isi Token dari 118

$response = $client->request('GET', 'list', [
    'headers' => [
        'Accept' => 'application/json',
        'Authorization' => 'Bearer '.$token,
    ],
]);

$body = $response->getBody();
$content = $body->getContents();

//echo $body;
header('Content-Type: application/json');
echo $content

//echo "<pre>";
//echo print_r($response->getBody()->getContents());
//echo "</pre>";

?>